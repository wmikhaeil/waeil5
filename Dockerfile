FROM python
COPY / helloworld.py.
WORKDIR /helloworld.py.
RUN pip install -r requirements.txt
ENTRYPOINT ["python"]
CMD ["helloworld.py"]